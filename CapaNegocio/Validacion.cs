﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CapaDatos;

namespace CapaNegocio
{
    public class Validacion
    {
        GestionVentas gestionVentas = new GestionVentas();

        public List<Venta> RetornarVentas()
        {
            return gestionVentas.ObtenerVentas();
        }
    }
}
