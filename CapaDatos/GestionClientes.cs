﻿using System;
using System.Collections.Generic;

namespace CapaDatos
{
    public class GestionClientes
    {
        
        List<Cliente> listaClientes;

        public GestionClientes()
        {
            listaClientes = new List<Cliente>();
            listaClientes = CrearProductos();
        }

        public void IngresarCliente(Cliente cliente)
        {
            listaClientes.Add(cliente);
        }

        private List<Cliente> CrearProductos()
        {
            List<Cliente> clientes = new List<Cliente>();
            Random random = new Random();
            int numero;
            for (int i = 1; i <= 9; i++)
            {

                Cliente cliente = new Cliente();
                cliente.Cedula = i.ToString()+ i.ToString()+ i.ToString()+ i.ToString()+ i.ToString()+ i.ToString();
                numero = random.Next(20);
                char letra = (char)(((int)'A') + random.Next(26));
                cliente.Nombres = "NOMBRE" + letra + numero + i;
                cliente.Apellidos = "APELLIDO" + letra + numero + i;
                clientes.Add(cliente);
            }

            return clientes;
        }
    }
}
