﻿using System;
namespace CapaDatos
{
    public class Producto
    {
        public int Identificador { get; set; }

        public string Descripcion { get; set; }

        public decimal Precio { get; set; }


        public Producto()
        {
        }
    }
}
