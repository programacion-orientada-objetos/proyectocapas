﻿using System;
using System.Collections.Generic;

namespace CapaDatos
{
    public class GestionProductos
    {
        List<Producto> listaProductos;
        public GestionProductos()
        {
            listaProductos = new List<Producto>();
            listaProductos = CrearProductos();
        }

        public void IngresarProducto(Producto producto)
        {
            listaProductos.Add(producto);
        }

        private List<Producto> CrearProductos()
        {
            List<Producto> productos = new List<Producto>();
            Random random = new Random();
            int numero;
            for (int i = 1; i <= 20; i++)
            {
                Producto producto = new Producto();
                producto.Identificador = i;
                numero = random.Next(20);
                char letra = (char)(((int)'A') + random.Next(26));
                producto.Descripcion = "PRODUCTO" + letra + numero + i;
                producto.Precio = numero;
                productos.Add(producto);
            }

            return productos;
        }
    }
}
